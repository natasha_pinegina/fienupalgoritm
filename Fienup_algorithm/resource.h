//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Fienup_algorithm.rc
//
#define IDD_FIENUP_ALGORITHM_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDC_A1                          1000
#define IDC_A2                          1001
#define IDC_A3                          1002
#define IDC_A4                          1003
#define IDC_A5                          1004
#define IDC_P1                          1005
#define IDC_P2                          1006
#define IDC_P3                          1007
#define IDC_P4                          1008
#define IDC_P5                          1009
#define IDC_t01                         1010
#define IDC_t02                         1011
#define IDC_t03                         1012
#define IDC_t04                         1013
#define IDC_t05                         1014
#define IDC_T                           1016
#define IDC_Get_Input_Signal            1017
#define IDC_Input_Signal                1018
#define IDC_BPF                         1019
#define IDC_Input_Signal2               1020
#define IDC_Spectr                      1020
#define IDC_nev                         1021
#define IDC_Reflected                   1022
#define IDC_Combined                    1023
#define IDC_Stop                        1024
#define IDC_Spectr2                     1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

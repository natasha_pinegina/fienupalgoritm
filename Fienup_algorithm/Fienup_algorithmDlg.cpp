
// Fienup_algorithmDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "Fienup_algorithm.h"
#include "Fienup_algorithmDlg.h"
#include "afxdialogex.h"
#include <ctime>
#include <cstdlib>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define KOORD(x,y) (xp*((x)-xmin)),(yp*((y)-ymax))
#define KOORDSPECTR(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))

#define rand_Max    2*3.14159

// ���������� ���� CFienup_algorithmDlg

int timer = 0;

DWORD dwThread;
HANDLE hThread;
DWORD WINAPI MyProc(PVOID pv)
{
	CFienup_algorithmDlg* p = (CFienup_algorithmDlg*)pv;
	p->CalculateVosstSignal();
	return 0;
}

CFienup_algorithmDlg::CFienup_algorithmDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFienup_algorithmDlg::IDD, pParent)
	, A1(4)
	, A2(2)
	, A3(3)
	, A4(2)
	, A5(2)
	, P1(10)
	, P2(5)
	, P3(7)
	, P4(5)
	, P5(10)
	, t01(150)
	, t02(300)
	, t03(380)
	, t04(520)
	, t05(720)
	, T(1024)
	, nev(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFienup_algorithmDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_A1, A1);
	DDX_Text(pDX, IDC_A2, A2);
	DDX_Text(pDX, IDC_A3, A3);
	DDX_Text(pDX, IDC_A4, A4);
	DDX_Text(pDX, IDC_A5, A5);
	DDX_Text(pDX, IDC_P1, P1);
	DDX_Text(pDX, IDC_P2, P2);
	DDX_Text(pDX, IDC_P3, P3);
	DDX_Text(pDX, IDC_P4, P4);
	DDX_Text(pDX, IDC_P5, P5);
	DDX_Text(pDX, IDC_t01, t01);
	DDX_Text(pDX, IDC_t02, t02);
	DDX_Text(pDX, IDC_t03, t03);
	DDX_Text(pDX, IDC_t04, t04);
	DDX_Text(pDX, IDC_t05, t05);
	DDX_Text(pDX, IDC_T, T);
	DDX_Text(pDX, IDC_nev, nev);
	DDX_Control(pDX, IDC_Reflected, Reflected);
	DDX_Control(pDX, IDC_Combined, Combined);
	DDX_Control(pDX, IDC_BPF, animation);
}

BEGIN_MESSAGE_MAP(CFienup_algorithmDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Get_Input_Signal, &CFienup_algorithmDlg::OnBnClickedGetInputSignal)
	ON_BN_CLICKED(IDC_BPF, &CFienup_algorithmDlg::OnBnClickedBpf)
	//ON_BN_CLICKED(IDC_Stop, &CFienup_algorithmDlg::OnBnClickedStop)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_Stop, &CFienup_algorithmDlg::OnBnClickedStop)
END_MESSAGE_MAP()


// ����������� ��������� CFienup_algorithmDlg

BOOL CFienup_algorithmDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

    // ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	PicWnd_Input_Sign = GetDlgItem(IDC_Input_Signal);
	PicDc_Input_Sign = PicWnd_Input_Sign->GetDC();
	PicWnd_Input_Sign->GetClientRect(&Pic_Input_Sign);

	PicWndSpectr = GetDlgItem(IDC_Spectr);
	PicDcSpectr = PicWndSpectr->GetDC();
	PicWndSpectr->GetClientRect(&PicSpectr);

	VosstSignalWnd = GetDlgItem(IDC_Spectr2);
	VosstSignalDc = VosstSignalWnd->GetDC();
	VosstSignalWnd->GetClientRect(&VosstSignalPic);

	setka_pen.CreatePen(		//��� �����
		PS_DOT,					//����������
		0.1,						//������� 1 �������
		RGB(0, 0, 250));		//����  ������
	osi_pen.CreatePen(			//������������ ���
		PS_SOLID,				//�������� �����
		3,						//������� 2 �������
		RGB(0, 0, 0));			//���� ������

	graf_pen.CreatePen(			//������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(0, 0, 255));			//���� ������
	graf_pen2.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(150, 150, 0));

	activated = 0;
	DrawVosstSignalFlag = false;
	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_Reflected));
	pcb1->SetCheck(1);
	

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CFienup_algorithmDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		if (DrawVosstSignalFlag == true)
		{
			Draw1Graph(
				new_signal,
				VosstSignalDc,
				VosstSignalPic,
				&graf_pen3,
				T,
				T,
				CString("x"),
				CString("Ampl"));
			if (DrawVosstSignalKoordFlag == true)
				DrawKoord(VosstSignalDc, VosstSignalPic, CString("x"), CString("Ampl"));
		}
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CFienup_algorithmDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CFienup_algorithmDlg::Input_Signal(int t)
{
	double Amplitude[] = { A1, A2, A3, A4, A5 };
	double Sigma[] = { P1, P2, P3, P4, P5};
	double Centers_of_Gaussian_domes[] = { t01, t02, t03, t04, t05 };
	double result = 0;
	for (int i = 0; i <= 4; i++)
	{
		result += Amplitude[i] * exp(-((t - Centers_of_Gaussian_domes[i]) / Sigma[i])*((t - Centers_of_Gaussian_domes[i]) / Sigma[i]));
	}
	return result;
}

void CFienup_algorithmDlg::Draw1Graph(double* Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, int KolToch, int AbsMax, CString Abs, CString Ord)
{
	// ����� ������������� � ������������ ��������
	xmin = Mass[0];
	xmax = Mass[0];
	for (int i = 1; i < KolToch; i++)
	{
		if (Mass[i] < xmin)
		{
			xmin = Mass[i];
		}
		if (Mass[i] > xmax)
		{
			xmax = Mass[i];
		}
	}
	//---- ��������� -------------------------------------------------
	// �������� ��������� ����������
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ ������� ���� ������� ����� ������ ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ ��������� ����� ��������� -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// ������������ ����� ����� ���������
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// �������������� ����� ����� ���������
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ ��������� ���� ----------------------------------------------------
	pen = MemDc->SelectObject(&koordpen);
	// ��������� ��� X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// ������� �� ��� X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	// ��������� ��� Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// ������� �� ��� Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ ������� ���� --------------------------------------------
	// ��������� ����������� ���� ������
	MemDc->SetBkMode(TRANSPARENT);
	// ��������� ������
	MemDc->SelectObject(&fontgraph);
	// ������� ��� X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// ������� ��� Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// ����� ������� ��� ���������
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// ���������
	pen = MemDc->SelectObject(graphpen);
	MemDc->MoveTo(xx0, yymax + (Mass[0] - xmin) / (xmax - xmin) * (yy0 - yymax));
	for (int i = 0; i < KolToch; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (KolToch - 1);
		yyi = yymax + (Mass[i] - xmin) / (xmax - xmin) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- ����� �������� �������� -----------------------------------
	// �� ��� �������
	for (int i = 6; i < 25; i += 5)
	{
		sprintf(znach, "%5.1f", (i - 1) * (float)AbsMax / 22);
		MemDc->TextOut(i * WinPic.Width() / 25 + 2, WinPic.Height() * 9 / 10 + 2, CString(znach));
	}
	// �� ��� �������
	sprintf(znach, "%5.2f", xmax);
	MemDc->TextOut(0, WinPic.Height() / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.75 * xmax);
	MemDc->TextOut(0, WinPic.Height() * 5 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.5 * xmax);
	MemDc->TextOut(0, WinPic.Height() * 9 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.25 * xmax);
	MemDc->TextOut(0, WinPic.Height() * 13 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.0);
	MemDc->TextOutW(0, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//----- ����� �� ����� -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CFienup_algorithmDlg::DrawKoord(CDC* WinDc, CRect WinPic, CString Abs, CString Ord)
{
	// �������� ��������� ����������
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ ������� ���� ������� ����� ������ ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ ��������� ����� ��������� -----------------------------------------
	pen = MemDc->SelectObject(&netkoordpen);
	// ������������ ����� ����� ���������
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// �������������� ����� ����� ���������
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ ��������� ���� ----------------------------------------------------
	pen = MemDc->SelectObject(&koordpen);
	// ��������� ��� X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// ������� �� ��� X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	// ��������� ��� Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// ������� �� ��� Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ ������� ���� --------------------------------------------
	// ��������� ����������� ���� ������
	MemDc->SetBkMode(TRANSPARENT);
	// ��������� ������
	MemDc->SelectObject(&fontgraph);
	// ������� ��� X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// ������� ��� Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	//----- ����� �� ����� -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CFienup_algorithmDlg::Pererisovka_Signala()
{

	xmin = -2;			//����������� �������� �
	xmax = T;			//������������ �������� �
	ymin = -max_signal_input / 10;		//����������� �������� y
	ymax = max_signal_input+1;//������������ �������� y


	xp = ((double)(Pic_Input_Sign.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(Pic_Input_Sign.Height()) / (ymax - ymin));

	PicDc_Input_Sign->FillSolidRect(&Pic_Input_Sign, RGB(250, 250, 250));			//���������� ��� 


	PicDc_Input_Sign->SelectObject(&osi_pen);
	//������ ��� Y
	PicDc_Input_Sign->MoveTo(KOORD(0, ymax));
	PicDc_Input_Sign->LineTo(KOORD(0, ymin));
	//������ ��� �
	PicDc_Input_Sign->MoveTo(KOORD(xmin, 0));
	PicDc_Input_Sign->LineTo(KOORD(xmax, 0));

	//������� ����
	PicDc_Input_Sign->TextOutW(KOORD(0, ymax - 0.2), _T("Y")); //Y
	PicDc_Input_Sign->TextOutW(KOORD(xmax - 0.3, 0 - 0.2), _T("t")); //X

	PicDc_Input_Sign->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin + 1; x <= xmax; x += xmax / 10)
	{
		PicDc_Input_Sign->MoveTo(KOORD(x, ymax));
		PicDc_Input_Sign->LineTo(KOORD(x, ymin));
	}
	//��������� ����� �� x
	for (float y = ymin + 1; y <= ymax; y += ymax / 5)
	{
		PicDc_Input_Sign->MoveTo(KOORD(xmin, y));
		PicDc_Input_Sign->LineTo(KOORD(xmax, y));
	}


	//������� ����� �� ���
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc_Input_Sign->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin; i <= ymax; i += ymax / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDc_Input_Sign->TextOutW(KOORD(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin; j <= xmax; j += xmax / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDc_Input_Sign->TextOutW(KOORD(j - 0.25, -0.2), str);
	}
}



void CFienup_algorithmDlg::Pererisovka_Spectra()
{

	xmin1 = -1;			//����������� �������� �
	xmax1 = T;			//������������ �������� �
	ymin1 = -max_spectr / 10;		//����������� �������� y
	ymax1 = max_spectr + 4;//������������ �������� y


	xp1 = ((double)(PicSpectr.Width()) / (xmax1 - xmin1));			//������������ ��������� ��������� �� �
	yp1 = -((double)(PicSpectr.Height()) / (ymax1 - ymin1));

	PicDcSpectr->FillSolidRect(&PicSpectr, RGB(250, 250, 250));			//���������� ��� 


	PicDcSpectr->SelectObject(&osi_pen);
	//������ ��� Y
	PicDcSpectr->MoveTo(KOORDSPECTR(0, ymax1));
	PicDcSpectr->LineTo(KOORDSPECTR(0, ymin1));
	//������ ��� �
	PicDcSpectr->MoveTo(KOORDSPECTR(xmin1, 0));
	PicDcSpectr->LineTo(KOORDSPECTR(xmax1, 0));

	//������� ����
	PicDcSpectr->TextOutW(KOORDSPECTR(0, ymax1 - 0.2), _T("Y")); //Y
	PicDcSpectr->TextOutW(KOORDSPECTR(xmax1 - 0.3, 0 - 0.2), _T("t")); //X

	PicDcSpectr->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin1 + 1; x <= xmax1; x += xmax1 / 10)
	{
		PicDcSpectr->MoveTo(KOORDSPECTR(x, ymax1));
		PicDcSpectr->LineTo(KOORDSPECTR(x, ymin1));
	}
	//��������� ����� �� x
	for (float y = ymin1 + 1; y <= ymax1; y += ymax1 / 5)
	{
		PicDcSpectr->MoveTo(KOORDSPECTR(xmin1, y));
		PicDcSpectr->LineTo(KOORDSPECTR(xmax1, y));
	}


	//������� ����� �� ���
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSpectr->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin1; i <= ymax1; i += ymax1 / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDcSpectr->TextOutW(KOORDSPECTR(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin1; j <= xmax1; j += xmax1 / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDcSpectr->TextOutW(KOORDSPECTR(j - 0.25, -0.2), str);
	}
}

void CFienup_algorithmDlg::OnBnClickedGetInputSignal()
{
	UpdateData(true);
	memset(signal_input, 0, (T+1000)*sizeof(double));
	for (int i = 0; i <T; i++)
	{
		signal_input[i] = Input_Signal(i);
		if (max_signal_input < signal_input[i])
			max_signal_input = signal_input[i];
	}
	Pererisovka_Signala();
	PicDc_Input_Sign->SelectObject(&graf_pen);
	PicDc_Input_Sign->MoveTo(KOORD(0, signal_input[0]));;
	for (int i = 0; i <T; i++)
	{
		PicDc_Input_Sign->LineTo(KOORD(i, signal_input[i]));
	}

	memset(spectr, 0, (T + 1000) * sizeof(double));
	//������ �������������� �����
	int is = -1;
	//���������� ����������� �������
	//�������� �����=������+���
	//������ �����=0
	cmplx* sp = new cmplx[T];
	for (int i = 0; i < T; i++)
	{
		sp[i].real = signal_input[i];
		sp[i].image = 0;
	}
	fourea(sp, T, is);
	//double Ei = 0;
	max_spectr = 0;
	E0 = 0;
	for (int i = 0; i < T; i++)
	{
		//��������� ������ ������� ������� |s|
		spectr[i] = sqrt(sp[i].real * sp[i].real + sp[i].image * sp[i].image);
		if (spectr[i] > max_spectr)
		{
			max_spectr = spectr[i];
		}
		//E0 += spectr[i] * spectr[i];//������ ������� �������
	}


	Pererisovka_Spectra();
	PicDcSpectr->SelectObject(&graf_pen2);
	PicDcSpectr->MoveTo(KOORDSPECTR(0, spectr[0]));
	for (int i = 0; i < T; i++)
	{
		PicDcSpectr->LineTo(KOORDSPECTR(i, spectr[i]));
	}
}

void CFienup_algorithmDlg::inverse()
{
	double* tempmas1=new double[T+1000];
	memset(tempmas1, 0, (T + 1000) * sizeof(double));
	for (int i = 0; i < T; i++)
		tempmas1[i] = new_signal[i];
	for (int i = 0; i < T / 2; i++)
		new_signal[i] = tempmas1[T / 2 - i - 1];
	for (int i = 0; i <T / 2; i++)
		new_signal[T / 2 + i] = tempmas1[T - i - 1];
}
void CFienup_algorithmDlg::CalculateVosstSignal()
{
	killtimer = false;
	double* mass_phase = new double[T + 1000];
	cmplx* new_sp = new cmplx[T];
	//������ ��������� ���
	//srand(time(0));
	srand(0);
	double f0 = 0, j = 0;
	//double nev = 0;
	while (j < 1000)
	{
		j++;
		//��� ������ �������� �����
		if (j == 1)
		{
			for (int i = 0; i < T; i++)
			{
				f0 = 2 * 3.14159265 * (double)rand() / (double)RAND_MAX;
				new_sp[i].real = spectr[i] * cos(f0);
				new_sp[i].image = spectr[i] * sin(f0);
			}
			FILE* results1;
			if (fopen_s(&results1, "����� � ������.txt", "w") == 0)
			{
				for (int i = 0; i < T; i++)
				{
					fprintf(results1, "%+*.1f ", 10, new_sp[i].real);
					fprintf(results1, "%+*.1f ", 10, new_sp[i].image);
					fprintf(results1, "\n");
				}
			}
		}
		memset(new_signal, 0, (T + 1000) * sizeof(double));
		//�������� �������������� �����
		fourea(new_sp, T, 1);
		for (int i = 0; i < T; i++)
		{
			//�������� �� ���������������
			if (new_sp[i].real < 0) new_sp[i].real = 0;
			new_signal[i] = new_sp[i].real;
			new_sp[i].image = 0;
			//nev += sqrt((signal_input[i] - new_signal[i]) * (signal_input[i] - new_signal[i]));
		}
		FILE* results2;
		if (fopen_s(&results2, "����� ������.txt", "w") == 0)
		{
			for (int i = 0; i < T; i++)
			{
				fprintf(results2, "%+*.1f ", 10, new_signal[i]);
				fprintf(results2, "\n");
			}
		}
		//Sleep(100);
		memset(mass_phase, 0, (T + 1000) * sizeof(double));
		//������ �������������� �����
		fourea(new_sp, T, -1);
		///////////////////////////////////////////////////////
		for (int i = 0; i < T; i++)
		{
			mass_phase[i] = atan2(new_sp[i].image, new_sp[i].real);
			new_sp[i].real = spectr[i] * cos(mass_phase[i]);
			new_sp[i].image = spectr[i] * sin(mass_phase[i]);
		}

		////������ ���������� ������
		//Pererisovka_Signala();
		//PicDc_Input_Sign->SelectObject(&graf_pen);
		//PicDc_Input_Sign->MoveTo(KOORD(0, signal_input[0]));
		//for (int i = 0; i < T; i++)
		//{
		//	PicDc_Input_Sign->LineTo(KOORD(i, signal_input[i]));
		//}

		if (Reflected.GetCheck())
		{
			double* signal = new double[T + 1000];
			memset(signal, 0, (T + 1000) * sizeof(double));
			inverse();
			/*for (int i = 0; i < T; i++)
			{
				signal[i]=new_signal[T - i];
			}
			for (int i = 0; i < T; i++)
			{
				new_signal[i] = signal[i];
			}*/
		}

		if (Combined.GetCheck())
		{
			//// ��� ������  ����� �������
			double* tempmas = new double[T];
			double* xnewt = new double[T];
			double* mas_nev = new double[T];
			double* mas_inv_nev = new double[T];

			memset(tempmas, 0, (T) * sizeof(double));
			memset(xnewt, 0, (T) * sizeof(double));
			memset(mas_nev, 0, (T) * sizeof(double));
			memset(mas_inv_nev, 0, (T) * sizeof(double));

			for (int i = 0; i < T; i++)
			{
				xnewt[i] = new_signal[i];
				tempmas[i] = new_signal[i];
				mas_nev[i] = 0;
				mas_inv_nev[i] = 0;
			}
			for (int j = 0; j < T; j++)
			{
				for (int i = 0; i < T; i++)
				{
					if ((i + j) >= T)
						xnewt[i] = tempmas[i + j - T];
					else
						xnewt[i] = tempmas[i + j];
				}
				for (int k = 0; k < T; k++)
					mas_nev[j] += sqrt((signal_input[k] - xnewt[k]) * (signal_input[k] - xnewt[k]));
			}
			inverse();
			for (int i = 0; i < T; i++)
			{
				xnewt[i] = new_signal[i];
				tempmas[i] = new_signal[i];
			}
			for (int j = 0; j < T; j++)
			{
				for (int i = 0; i < T; i++)
				{
					if ((i + j) >= T)
						xnewt[i] = tempmas[i + j - T];
					else
						xnewt[i] = tempmas[i + j];
				}
				for (int k = 0; k < T; k++)
					mas_inv_nev[j] += sqrt((signal_input[k] - xnewt[k]) * (signal_input[k] - xnewt[k]));
			}
			inverse();

			double min = mas_nev[0];
			int min1 = 0;
			double mininv = mas_inv_nev[0];
			int min2 = 0;
			for (int i = 1; i < T; i++)
			{
				if (mas_nev[i] < min)
				{
					min = mas_nev[i];
					min1 = i;
				}
				if (mas_inv_nev[i] < mininv)
				{
					mininv = mas_inv_nev[i];
					min2 = i;
				}
			}

			if (mininv < min)
			{
				inverse();
				for (int i = 0; i < T; i++)
					tempmas[i] = new_signal[i];
				for (int i = 0; i < T; i++)
				{
					if ((i + min2) >= T)
						new_signal[i] = tempmas[i + min2 - T];
					else
						new_signal[i] = tempmas[i + min2];
				}
			}
			else
			{
				for (int i = 0; i < T; i++)
					tempmas[i] = new_signal[i];
				for (int i = 0; i < T; i++)
				{
					if ((i + min1) >= T)
						new_signal[i] = tempmas[i + min1 - T];
					else
						new_signal[i] = tempmas[i + min1];
				}
			}
		}

		/*PicDc_Input_Sign->SelectObject(&graf_pen3);
		PicDc_Input_Sign->MoveTo(KOORD(0, new_signal[0]));*/
		for (int i = 0; i < T; i++)
		{
			//PicDc_Input_Sign->LineTo(KOORD(i, new_signal[i]));
			nev += sqrt((signal_input[i] - new_signal[i]) * (signal_input[i] - new_signal[i]));
		}
		nev = nev / T;
		Sleep(5);
	}
	PicDc_Input_Sign->SelectObject(&graf_pen3);
	PicDc_Input_Sign->MoveTo(KOORD(0, new_signal[0])); 
		for (int i = 0; i < T; i++)
		{
			PicDc_Input_Sign->LineTo(KOORD(i, new_signal[i]));
			//nev += sqrt((signal_input[i] - new_signal[i]) * (signal_input[i] - new_signal[i]));
		}
	killtimer = true;
}

void CFienup_algorithmDlg::OnBnClickedBpf()
{
	DrawVosstSignalKoordFlag = false;
	DrawVosstSignalFlag = true;
	hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
	timer = SetTimer(1, 1, NULL);
	UpdateData(false);
}

void CFienup_algorithmDlg::OnTimer(UINT_PTR nIDEvent)
{
	Invalidate(0);
	if (killtimer)
	{
		KillTimer(timer);
		UpdateData(false);
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CFienup_algorithmDlg::OnBnClickedStop()
{
	switch (activated)
	{
	case 0:
		KillTimer(1);
		GetDlgItem(IDC_Stop)->SetWindowTextW(L"��������� ��������");
		SuspendThread(hThread);
		activated = 1;
		break;
	case 1:
		SetTimer(1, 1, NULL);
		GetDlgItem(IDC_Stop)->SetWindowTextW(L"���������� ��������");
		ResumeThread(hThread);
		activated = 0;
		break;
	}
}


// Fienup_algorithmDlg.h : ���� ���������
//

#pragma once


// ���������� ���� CFienup_algorithmDlg
class CFienup_algorithmDlg : public CDialogEx
{
// ��������
public:
	CFienup_algorithmDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_FIENUP_ALGORITHM_DIALOG };

	CWnd * PicWnd_Input_Sign;
	CDC * PicDc_Input_Sign;
	CRect Pic_Input_Sign;

	CWnd * PicWndSpectr;
	CDC * PicDcSpectr;
	CRect PicSpectr;

	CWnd* VosstSignalWnd;
	CDC* VosstSignalDc;
	CRect VosstSignalPic;


	CPen koordpen, netkoordpen, signalpen, spectrpen, vsignalpen;
	CPen* pen;
	CFont fontgraph;
	CFont* font;

	CPen osi_pen;		// ��� ���� 
	CPen setka_pen;		// ��� �����
	CPen graf_pen;		// ��� ������� �������
	CPen graf_pen2;
	CPen graf_pen3;

	double
		xx0,
		xxmax,
		yy0,
		yymax,
		xxi,
		yyi,
		iter;

	char
		znach[1000];

	double xp, yp,			//����������� ���������
		xmin, xmax,			//�������������� � ����������� �������� � 
		ymin, ymax;			//�������������� � ����������� �������� y

	double xp1, yp1,			//����������� ���������
		xmin1, xmax1,			//�������������� � ����������� �������� � 
		ymin1, ymax1;			//�������������� � ����������� �������� y

	void Draw1Graph(double*, CDC*, CRect, CPen*, int, int, CString, CString);
	void DrawKoord(CDC*, CRect, CString, CString);

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// ��������� �������
	double A1;
	double A2;
	double A3;
	double A4;
	double A5;
	// ���������
	double P1;
	double P2;
	double P3;
	double P4;
	double P5;
	// ��������� �������
	double t01;
	double t02;
	double t03;
	double t04;
	double t05;
	// ������������ �������
	int T;
	//bool activated;
	double max_signal_input = 0;
	double max_spectr = 0;
	double E0;
	bool killtimer, DrawVosstSignalFlag, DrawVosstSignalKoordFlag;
	afx_msg double Input_Signal(int t);
	double *signal_input = new double[T+1000];
	double *spectr = new double[T+1000];
	double* new_spectr = new double[T + 1000];
	double* new_signal = new double[T + 1000];
	double* restored_signal = new double[T + 1000];
	afx_msg void OnBnClickedGetInputSignal();
	afx_msg void Pererisovka_Signala();
	afx_msg void Pererisovka_Spectra();
	afx_msg void inverse();
	afx_msg void CalculateVosstSignal();

	typedef struct cmplx { float real; float image; } Cmplx;
	//========================================================

	void fourea(struct cmplx *data, int n, int is)
	{
		int i, j, istep;
		int m, mmax;
		float r, r1, theta, w_r, w_i, temp_r, temp_i;
		float pi = 3.1415926f;

		r = pi*is;
		j = 0;
		for (i = 0; i<n; i++)
		{
			if (i<j)
			{
				temp_r = data[j].real;
				temp_i = data[j].image;
				data[j].real = data[i].real;
				data[j].image = data[i].image;
				data[i].real = temp_r;
				data[i].image = temp_i;
			}
			m = n >> 1;
			while (j >= m) { j -= m; m = (m + 1) / 2; }
			j += m;
		}
		mmax = 1;
		while (mmax<n)
		{
			istep = mmax << 1;
			r1 = r / (float)mmax;
			for (m = 0; m<mmax; m++)
			{
				theta = r1*m;
				w_r = (float)cos((double)theta);
				w_i = (float)sin((double)theta);
				for (i = m; i<n; i += istep)
				{
					j = i + mmax;
					temp_r = w_r*data[j].real - w_i*data[j].image;
					temp_i = w_r*data[j].image + w_i*data[j].real;
					data[j].real = data[i].real - temp_r;
					data[j].image = data[i].image - temp_i;
					data[i].real += temp_r;
					data[i].image += temp_i;
				}
			}
			mmax = istep;
		}
		if (is>0)
			for (i = 0; i<n; i++)
			{
				data[i].real /= (float)n;
				data[i].image /= (float)n;
			}

	}
	afx_msg void OnBnClickedBpf();
	double nev;
	// ��������� ���������� ������
	CButton Reflected;
	// ����������� ������
	CButton Combined;
	//afx_msg void OnBnClickedStop();
	CButton animation;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedStop();
	int activated;
};
